import React from "react";
import "./App.css";

function Text(props) {
    return (
    <h1>Hello, <span style = {{color: props.color}}>{props.name}</span></h1>
    );
  }

function App() {
    return (
        <Text name = "Misty"  color = "gray"/>
    )
}

export default App;